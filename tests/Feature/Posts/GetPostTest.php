<?php

namespace Tests\Feature\Posts;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Http\Response;
use App\Models\Post;
use Illuminate\Testing\Fluent\AssertableJson;

class GetPostTest extends TestCase
{
    /** @test */
    public function user_can_get_post_if_post_exists()
    {
        $post = Post::factory()->create();
        $response = $this->getJson(route('post.show', $post->id));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(fn (AssertableJson $json) =>
        $json->has('message')->has('data'));
    }
}
